{
    "id": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c1134212-b6fd-492b-8e7e-a285f0700ea0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "compositeImage": {
                "id": "1aa09102-4118-47f8-bf2b-ea56439035b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1134212-b6fd-492b-8e7e-a285f0700ea0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a224d257-21fe-4ee2-8489-6322e0f9a711",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1134212-b6fd-492b-8e7e-a285f0700ea0",
                    "LayerId": "0455dc3f-cb29-4185-a58c-23552f68b757"
                }
            ]
        },
        {
            "id": "78b51abd-5223-4e25-ad3b-b37412a108f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "compositeImage": {
                "id": "8013b13e-2dc5-4734-9f81-5d7c80803987",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "78b51abd-5223-4e25-ad3b-b37412a108f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b2e297ea-f068-40b4-8704-afeb50b180fe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "78b51abd-5223-4e25-ad3b-b37412a108f4",
                    "LayerId": "0455dc3f-cb29-4185-a58c-23552f68b757"
                }
            ]
        },
        {
            "id": "e64d7924-e564-47ec-90a9-7cf2d4fc3cd2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "compositeImage": {
                "id": "5369bbac-e52b-4d26-b4ab-d2c4d4b396b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e64d7924-e564-47ec-90a9-7cf2d4fc3cd2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "563a6678-73f5-4bf1-8c1a-9da3cabdbd7c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e64d7924-e564-47ec-90a9-7cf2d4fc3cd2",
                    "LayerId": "0455dc3f-cb29-4185-a58c-23552f68b757"
                }
            ]
        },
        {
            "id": "4b088513-f23b-4d6d-be6b-ee21a768694f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "compositeImage": {
                "id": "95b967df-7f74-4333-af72-728c8a249cda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b088513-f23b-4d6d-be6b-ee21a768694f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c38c32d6-61cc-4f67-8ce0-8f4142005e5f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b088513-f23b-4d6d-be6b-ee21a768694f",
                    "LayerId": "0455dc3f-cb29-4185-a58c-23552f68b757"
                }
            ]
        },
        {
            "id": "1bef9a0b-2046-41e6-afde-b20af422366b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "compositeImage": {
                "id": "7162156e-0a9f-4258-9d49-adf243ec5e7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1bef9a0b-2046-41e6-afde-b20af422366b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a589a435-cce6-4343-9bab-f2bc04c92920",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1bef9a0b-2046-41e6-afde-b20af422366b",
                    "LayerId": "0455dc3f-cb29-4185-a58c-23552f68b757"
                }
            ]
        },
        {
            "id": "f774c49e-101d-4080-aa96-d34d9477e4a5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "compositeImage": {
                "id": "5d934162-6039-4c83-9717-b8c83aa6183f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f774c49e-101d-4080-aa96-d34d9477e4a5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e946834a-b471-450b-9f02-7f5cbd8ca530",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f774c49e-101d-4080-aa96-d34d9477e4a5",
                    "LayerId": "0455dc3f-cb29-4185-a58c-23552f68b757"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "0455dc3f-cb29-4185-a58c-23552f68b757",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "65a16d15-ca15-4e2d-8158-bf8410cf2aab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 24
}