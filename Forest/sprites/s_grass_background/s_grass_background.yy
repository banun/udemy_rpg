{
    "id": "9f9372b9-b66a-436c-a725-7f050f07e905",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_grass_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "578f6514-0d7b-4b39-b483-fcd36ad9fc03",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9f9372b9-b66a-436c-a725-7f050f07e905",
            "compositeImage": {
                "id": "1c40ace5-7f20-4457-adb0-69371cc6f05e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "578f6514-0d7b-4b39-b483-fcd36ad9fc03",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d4ba4562-badb-4b7f-8797-be6dfec9a6fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "578f6514-0d7b-4b39-b483-fcd36ad9fc03",
                    "LayerId": "4d8d14cb-2374-4c67-b28c-a35380db7bd5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "4d8d14cb-2374-4c67-b28c-a35380db7bd5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9f9372b9-b66a-436c-a725-7f050f07e905",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}