{
    "id": "1e12f74a-e35a-4610-97bc-78a1bbf79160",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack_up",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 40,
    "bbox_left": 3,
    "bbox_right": 44,
    "bbox_top": 9,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "9c45250b-95eb-4639-b7ca-ec627035c67f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e12f74a-e35a-4610-97bc-78a1bbf79160",
            "compositeImage": {
                "id": "8fce6e10-89f5-48d9-ae1f-929302b36d3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9c45250b-95eb-4639-b7ca-ec627035c67f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c94fc595-2187-42af-b27b-6586c2f0b751",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9c45250b-95eb-4639-b7ca-ec627035c67f",
                    "LayerId": "412b4fa2-5580-4913-b74a-12e439fb750b"
                }
            ]
        },
        {
            "id": "c1eabb92-8888-4975-9bab-995b44216cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e12f74a-e35a-4610-97bc-78a1bbf79160",
            "compositeImage": {
                "id": "c0357192-f0aa-4119-ad27-e6a0ab8c8b8b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1eabb92-8888-4975-9bab-995b44216cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ec51524-1516-465c-9f94-9388d7c70a4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1eabb92-8888-4975-9bab-995b44216cca",
                    "LayerId": "412b4fa2-5580-4913-b74a-12e439fb750b"
                }
            ]
        },
        {
            "id": "d4d07b88-5b99-4e36-806c-52f8fdd04cfa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e12f74a-e35a-4610-97bc-78a1bbf79160",
            "compositeImage": {
                "id": "ca183eb0-a1ef-4e22-bae7-0560487af265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4d07b88-5b99-4e36-806c-52f8fdd04cfa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21278411-4c5f-4607-9976-8cb0a800f28b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4d07b88-5b99-4e36-806c-52f8fdd04cfa",
                    "LayerId": "412b4fa2-5580-4913-b74a-12e439fb750b"
                }
            ]
        },
        {
            "id": "3d14cfd4-bc9b-4bac-a77c-f723e655bdab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1e12f74a-e35a-4610-97bc-78a1bbf79160",
            "compositeImage": {
                "id": "3c578d85-c2eb-46eb-8a28-a13d8a390fd8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3d14cfd4-bc9b-4bac-a77c-f723e655bdab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2757d1f-7f0e-4843-a0f9-2ff0a16c4d74",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3d14cfd4-bc9b-4bac-a77c-f723e655bdab",
                    "LayerId": "412b4fa2-5580-4913-b74a-12e439fb750b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "412b4fa2-5580-4913-b74a-12e439fb750b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1e12f74a-e35a-4610-97bc-78a1bbf79160",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 38
}