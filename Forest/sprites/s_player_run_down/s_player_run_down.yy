{
    "id": "8554c909-26e5-4563-9db5-830c219e75a1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "960c2559-f4a1-498e-8cf2-bff8ef46a25a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "compositeImage": {
                "id": "d7232d3f-e78d-47b0-bd7b-a5f1627a6553",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "960c2559-f4a1-498e-8cf2-bff8ef46a25a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52385599-4586-4d0f-b76a-f8b03f35c7b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "960c2559-f4a1-498e-8cf2-bff8ef46a25a",
                    "LayerId": "d808c245-0eee-47fc-9489-0ff323c0287f"
                }
            ]
        },
        {
            "id": "bcc518b7-ffa6-4b11-bc29-139b422fcfa5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "compositeImage": {
                "id": "59cb73b2-ae29-483a-8cf9-2b522f4214d8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcc518b7-ffa6-4b11-bc29-139b422fcfa5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bba28c3b-fa0b-4277-a0ed-6e86cf99ce87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcc518b7-ffa6-4b11-bc29-139b422fcfa5",
                    "LayerId": "d808c245-0eee-47fc-9489-0ff323c0287f"
                }
            ]
        },
        {
            "id": "59334416-e332-4449-b215-c6d02078dc32",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "compositeImage": {
                "id": "00bf17bc-ca55-4678-8656-39a4f0132d25",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59334416-e332-4449-b215-c6d02078dc32",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25bf0479-1d19-4715-85b6-86428e7b08bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59334416-e332-4449-b215-c6d02078dc32",
                    "LayerId": "d808c245-0eee-47fc-9489-0ff323c0287f"
                }
            ]
        },
        {
            "id": "d8f09af7-6327-42c0-9a32-ef8d5c3fa75f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "compositeImage": {
                "id": "4a11954b-8fed-4c81-b765-e24e6b2217e9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8f09af7-6327-42c0-9a32-ef8d5c3fa75f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75decff3-dadb-400e-8ea8-395eb53f104b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8f09af7-6327-42c0-9a32-ef8d5c3fa75f",
                    "LayerId": "d808c245-0eee-47fc-9489-0ff323c0287f"
                }
            ]
        },
        {
            "id": "b65d266f-71da-4f44-baea-4b962cedd333",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "compositeImage": {
                "id": "276bb630-939e-4178-a09a-2497515e707c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b65d266f-71da-4f44-baea-4b962cedd333",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4b66a6a-91d7-4517-828f-edad45c25f5c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b65d266f-71da-4f44-baea-4b962cedd333",
                    "LayerId": "d808c245-0eee-47fc-9489-0ff323c0287f"
                }
            ]
        },
        {
            "id": "f76b11ba-f1d4-45e6-a200-0146afcb381a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "compositeImage": {
                "id": "058f8385-8f29-4bf1-a03e-77c4059b090d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f76b11ba-f1d4-45e6-a200-0146afcb381a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f543fe53-e564-4fea-b5cd-46e9aafed1bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f76b11ba-f1d4-45e6-a200-0146afcb381a",
                    "LayerId": "d808c245-0eee-47fc-9489-0ff323c0287f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "d808c245-0eee-47fc-9489-0ff323c0287f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 24
}