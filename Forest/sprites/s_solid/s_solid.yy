{
    "id": "3b147dd9-7258-49fe-ab94-2bb9c4ac4e13",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_solid",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "c423c825-1dbc-442f-9376-15d23593deec",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3b147dd9-7258-49fe-ab94-2bb9c4ac4e13",
            "compositeImage": {
                "id": "5d34db40-beae-4186-97e3-2e29df826ab4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c423c825-1dbc-442f-9376-15d23593deec",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6f3dfe79-940f-4489-af07-ebeaab05d19c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c423c825-1dbc-442f-9376-15d23593deec",
                    "LayerId": "03cba5c4-1499-4f72-b862-f5f8d44d165d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "03cba5c4-1499-4f72-b862-f5f8d44d165d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3b147dd9-7258-49fe-ab94-2bb9c4ac4e13",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}