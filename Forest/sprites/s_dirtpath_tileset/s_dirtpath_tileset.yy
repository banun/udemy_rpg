{
    "id": "b9922bd2-7336-4bf5-82ce-7c97448c3dbb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dirtpath_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 143,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "69a3dbb0-bba6-4154-b81a-4269cbdb150f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9922bd2-7336-4bf5-82ce-7c97448c3dbb",
            "compositeImage": {
                "id": "da256fee-bab3-4c17-b22e-40ad7c46383c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "69a3dbb0-bba6-4154-b81a-4269cbdb150f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c033ee51-5c0a-402f-b784-b93962b3179e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "69a3dbb0-bba6-4154-b81a-4269cbdb150f",
                    "LayerId": "525c7248-fc1f-494c-b213-3f99f22e3141"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "525c7248-fc1f-494c-b213-3f99f22e3141",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9922bd2-7336-4bf5-82ce-7c97448c3dbb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 144,
    "xorig": 0,
    "yorig": 0
}