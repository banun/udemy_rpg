{
    "id": "29d3bacb-21f8-487e-b675-917c8efc5b6d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 41,
    "bbox_left": 3,
    "bbox_right": 41,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "2ae67ac5-ed33-4f4d-91e6-d728a89fea7c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29d3bacb-21f8-487e-b675-917c8efc5b6d",
            "compositeImage": {
                "id": "c040aa91-2c2e-4c3a-a9bb-a10d90b1e22f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ae67ac5-ed33-4f4d-91e6-d728a89fea7c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cf89e300-cb20-42b9-9888-9a4bee73eb8c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ae67ac5-ed33-4f4d-91e6-d728a89fea7c",
                    "LayerId": "c2c664c3-2877-4978-a716-f98d1227e5f4"
                }
            ]
        },
        {
            "id": "40373e10-bd31-4ce3-9844-33914b99ca75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29d3bacb-21f8-487e-b675-917c8efc5b6d",
            "compositeImage": {
                "id": "716297b0-46b6-4611-a9b1-29760dae007f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40373e10-bd31-4ce3-9844-33914b99ca75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "547af84f-cba8-4069-91df-c5956259533e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40373e10-bd31-4ce3-9844-33914b99ca75",
                    "LayerId": "c2c664c3-2877-4978-a716-f98d1227e5f4"
                }
            ]
        },
        {
            "id": "b1f831e8-35ab-44fc-b065-f12ad92c2d8d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29d3bacb-21f8-487e-b675-917c8efc5b6d",
            "compositeImage": {
                "id": "37637da8-8f22-46ba-8791-74a116327a7d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1f831e8-35ab-44fc-b065-f12ad92c2d8d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "446d1573-0f14-40ee-8ca3-027e61879775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1f831e8-35ab-44fc-b065-f12ad92c2d8d",
                    "LayerId": "c2c664c3-2877-4978-a716-f98d1227e5f4"
                }
            ]
        },
        {
            "id": "d8545a06-c69e-4bce-ab9a-abfb5d3e52e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "29d3bacb-21f8-487e-b675-917c8efc5b6d",
            "compositeImage": {
                "id": "4ea1ff36-70ef-4068-a61a-f57364e459e8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8545a06-c69e-4bce-ab9a-abfb5d3e52e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ceef116-78cf-48c1-886d-65390a9b7b82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8545a06-c69e-4bce-ab9a-abfb5d3e52e8",
                    "LayerId": "c2c664c3-2877-4978-a716-f98d1227e5f4"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "c2c664c3-2877-4978-a716-f98d1227e5f4",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "29d3bacb-21f8-487e-b675-917c8efc5b6d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 14,
    "yorig": 33
}