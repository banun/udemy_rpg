{
    "id": "ec48ff0e-e861-4443-92de-92af845f2e7e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_attack_down",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 2,
    "bbox_right": 41,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e5cc77e0-78e4-4fdb-8deb-d628c06072ab",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec48ff0e-e861-4443-92de-92af845f2e7e",
            "compositeImage": {
                "id": "1d75fcd8-2381-4998-b2d8-25ac56fdb30e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5cc77e0-78e4-4fdb-8deb-d628c06072ab",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c1c39584-0751-49e5-a35b-d3f24f594d4c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5cc77e0-78e4-4fdb-8deb-d628c06072ab",
                    "LayerId": "6ea95316-324e-41e3-b212-4ebd650fad3d"
                }
            ]
        },
        {
            "id": "7339ce70-f810-4160-8235-172157a15ddd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec48ff0e-e861-4443-92de-92af845f2e7e",
            "compositeImage": {
                "id": "0f7576f5-52ab-48c8-ae56-4eb497e5cb8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7339ce70-f810-4160-8235-172157a15ddd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5dcd9cb9-18e8-4c12-b539-1f20cea27310",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7339ce70-f810-4160-8235-172157a15ddd",
                    "LayerId": "6ea95316-324e-41e3-b212-4ebd650fad3d"
                }
            ]
        },
        {
            "id": "ee44f972-b748-4669-ae18-140b8b6cd0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec48ff0e-e861-4443-92de-92af845f2e7e",
            "compositeImage": {
                "id": "80c3633d-16d8-4479-9992-34e35a0799df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ee44f972-b748-4669-ae18-140b8b6cd0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b8c3d23-333f-405c-a585-0011a42f5da4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ee44f972-b748-4669-ae18-140b8b6cd0b5",
                    "LayerId": "6ea95316-324e-41e3-b212-4ebd650fad3d"
                }
            ]
        },
        {
            "id": "691ffc66-8623-4f46-8ffc-72582eacbc39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ec48ff0e-e861-4443-92de-92af845f2e7e",
            "compositeImage": {
                "id": "ac5400d8-2f10-481a-a9d7-610a6c0a2265",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "691ffc66-8623-4f46-8ffc-72582eacbc39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c7be307-1c94-4f84-8a9b-8f62fbcfb9b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "691ffc66-8623-4f46-8ffc-72582eacbc39",
                    "LayerId": "6ea95316-324e-41e3-b212-4ebd650fad3d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "6ea95316-324e-41e3-b212-4ebd650fad3d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ec48ff0e-e861-4443-92de-92af845f2e7e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 23,
    "yorig": 27
}