{
    "id": "39ace2cf-ab5b-406a-890c-a1bf1ade5815",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_dirtwall_tileset",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 32,
    "bbox_right": 287,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "a40972e7-4f60-4196-8f50-f489fecd4c82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "39ace2cf-ab5b-406a-890c-a1bf1ade5815",
            "compositeImage": {
                "id": "027a5b1f-6562-4fc2-860f-35782e94ea04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a40972e7-4f60-4196-8f50-f489fecd4c82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f4ca2d7f-f974-4029-bdaa-8b2be72b6cc5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a40972e7-4f60-4196-8f50-f489fecd4c82",
                    "LayerId": "713db008-6089-4aca-ade2-b472b2a539da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "713db008-6089-4aca-ade2-b472b2a539da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "39ace2cf-ab5b-406a-890c-a1bf1ade5815",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 288,
    "xorig": 0,
    "yorig": 0
}