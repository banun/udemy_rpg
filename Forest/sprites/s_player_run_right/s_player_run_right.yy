{
    "id": "159a9846-558f-4614-ae21-e830298d0202",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_player_run_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 24,
    "bbox_left": 5,
    "bbox_right": 19,
    "bbox_top": 15,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "frames": [
        {
            "id": "e04164d0-3c95-4588-876f-064f27079e77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "compositeImage": {
                "id": "b3e61821-e44f-4c2f-9137-dffa33db639d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e04164d0-3c95-4588-876f-064f27079e77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15077221-2837-4219-8aea-33c2e6f12d4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e04164d0-3c95-4588-876f-064f27079e77",
                    "LayerId": "2caf8cd6-e99b-4509-aa7d-03606a69ca32"
                }
            ]
        },
        {
            "id": "225c77d9-181b-4b41-a9f7-1c20473b9a10",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "compositeImage": {
                "id": "b66ccbd7-5df4-4d1d-a235-ce1311c86404",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "225c77d9-181b-4b41-a9f7-1c20473b9a10",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5e8ce8b7-c87b-489f-bded-e2fe77857ce4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "225c77d9-181b-4b41-a9f7-1c20473b9a10",
                    "LayerId": "2caf8cd6-e99b-4509-aa7d-03606a69ca32"
                }
            ]
        },
        {
            "id": "1f001ff3-7225-4028-a709-a8197193fcdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "compositeImage": {
                "id": "78bbc579-6622-4492-83a1-73ebcd358f40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1f001ff3-7225-4028-a709-a8197193fcdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "90f5d1ab-11c0-499b-abb6-89b971504a11",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1f001ff3-7225-4028-a709-a8197193fcdf",
                    "LayerId": "2caf8cd6-e99b-4509-aa7d-03606a69ca32"
                }
            ]
        },
        {
            "id": "36488896-f3a8-4975-b524-56b313f631e8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "compositeImage": {
                "id": "5f6bc7cd-febc-48dc-a1ac-989823ae4179",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36488896-f3a8-4975-b524-56b313f631e8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f259efc4-c2e1-4cb7-a235-85ac0fabade1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36488896-f3a8-4975-b524-56b313f631e8",
                    "LayerId": "2caf8cd6-e99b-4509-aa7d-03606a69ca32"
                }
            ]
        },
        {
            "id": "05845e24-5ede-43b1-887d-a810c0ea1685",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "compositeImage": {
                "id": "b57b7d88-fa7f-4823-82e4-181ff1c071ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "05845e24-5ede-43b1-887d-a810c0ea1685",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8140ff3-7ef7-4aad-9d3d-c88b24c60e40",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "05845e24-5ede-43b1-887d-a810c0ea1685",
                    "LayerId": "2caf8cd6-e99b-4509-aa7d-03606a69ca32"
                }
            ]
        },
        {
            "id": "936ff989-515f-4fc3-919d-a427a32009d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "compositeImage": {
                "id": "bc27054c-992d-444e-980c-083be8b3956f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "936ff989-515f-4fc3-919d-a427a32009d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ea7c262-b7c0-486b-8f2d-be0032a0cda2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "936ff989-515f-4fc3-919d-a427a32009d1",
                    "LayerId": "2caf8cd6-e99b-4509-aa7d-03606a69ca32"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "2caf8cd6-e99b-4509-aa7d-03606a69ca32",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "159a9846-558f-4614-ae21-e830298d0202",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 12,
    "yorig": 24
}