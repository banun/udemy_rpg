///@arg input_direction

var _input_direction = argument0;

 // round
direction_facing_ = clamp(round(_input_direction / 90), 0, 3);