{
    "id": "80c818dd-97db-43eb-b067-557c4f0046ff",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_player",
    "eventList": [
        {
            "id": "6152e278-d730-4595-89a7-5017035b75bd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "80c818dd-97db-43eb-b067-557c4f0046ff"
        },
        {
            "id": "8e2d6d3e-15d5-433e-9d42-ea94f960978c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "80c818dd-97db-43eb-b067-557c4f0046ff"
        },
        {
            "id": "155a1f53-2ff4-4608-aa2b-9398a368a739",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "80c818dd-97db-43eb-b067-557c4f0046ff"
        },
        {
            "id": "b545731c-813c-494a-bc0c-55ef53a23069",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 7,
            "m_owner": "80c818dd-97db-43eb-b067-557c4f0046ff"
        },
        {
            "id": "893f9961-d382-46bd-b448-fb0190312644",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 7,
            "m_owner": "80c818dd-97db-43eb-b067-557c4f0046ff"
        }
    ],
    "maskSpriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "solid": false,
    "spriteId": "8554c909-26e5-4563-9db5-830c219e75a1",
    "visible": true
}