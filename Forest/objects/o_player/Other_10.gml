/// @description Move State
var _animation_speed = 0.6;
var _x_input = keyboard_check(vk_right) - keyboard_check(vk_left);
var _y_input = keyboard_check(vk_down) - keyboard_check(vk_up);	// down is positive
var _input_direction = point_direction(0, 0, _x_input, _y_input); // comparison will always be unit vector
// only on first press
var _attack_input = keyboard_check_pressed(vk_space);

if _x_input == 0 and _y_input == 0 {
	sprite_index = 0;
	image_speed = 0;
	apply_friction_to_movement_entity();
} else {
	get_direction_facing(_input_direction);
	add_movement_maxspeed(_input_direction, acceleration_, max_speed_);
	image_speed = _animation_speed;
	if _x_input != 0 {
		image_xscale = _x_input;
	}
}

if (_attack_input == true) {
	state_ = player.sword;
}

move_movement_entity(false);